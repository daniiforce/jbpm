package com.mega.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 3)
	private String landscape; // (nvarchar) 3

	@Column(name = "emp_id", length = 8)
	private String empId; // (nvarchar) 8

	@Column(length = 8)
	private String start_date; // (nvarchar) 8

	@Column(length = 8)
	private String end_date; // (nvarchar) 8

	@Column(length = 2)
	private String address_type; // (nvarchar) 2

	@Column(length = 4)
	private String seq; // (nvarchar) 4

	@Column(length = 200)
	private String street; // (nvarchar) 200

	@Column(length = 30)
	private String location; // (nvarchar) 30

	@Column(length = 2)
	private String province; // (nvarchar) 2

	@Column(length = 3)
	private String country; // (nvarchar) 3

	@Column(length = 5)
	private String postalcode; // (nvarchar) 5

	@Column(length = 50)
	private String contact_person; // (nvarchar) 50

	@Column(length = 25)
	private String telp_num; // (nvarchar) 25

	@Column(length = 25)
	private String cellphone_num; // (nvarchar) 25

	@Column(length = 8)
	private String user_change; // (nvarchar) 8

	@Column(length = 14)
	private String last_change; // (nvarchar) 14

	@Column(length = 8)
	private String created_by; // (nvarchar) 8

	@Column(length = 14)
	private String created_date; // (nvarchar) 14

	@Column(length = 50)
	private String kecamatan; // (nvarchar) 50

	@Column(length = 50)
	private String kelurahan; // (nvarchar) 50

	@Column(length = 50)
	private String rt; // (nvarchar) 50

	@Column(length = 50)
	private String rw; // (nvarchar) 50

	@Column(length = 50)
	private String city; // (nvarchar) 50

	@Column(length = 50)
	private String kepemilikan; // (nvarchar) 50

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLandscape() {
		return landscape;
	}

	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getAddress_type() {
		return address_type;
	}

	public void setAddress_type(String address_type) {
		this.address_type = address_type;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getContact_person() {
		return contact_person;
	}

	public void setContact_person(String contact_person) {
		this.contact_person = contact_person;
	}

	public String getTelp_num() {
		return telp_num;
	}

	public void setTelp_num(String telp_num) {
		this.telp_num = telp_num;
	}

	public String getCellphone_num() {
		return cellphone_num;
	}

	public void setCellphone_num(String cellphone_num) {
		this.cellphone_num = cellphone_num;
	}

	public String getUser_change() {
		return user_change;
	}

	public void setUser_change(String user_change) {
		this.user_change = user_change;
	}

	public String getLast_change() {
		return last_change;
	}

	public void setLast_change(String last_change) {
		this.last_change = last_change;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public String getKecamatan() {
		return kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getRt() {
		return rt;
	}

	public void setRt(String rt) {
		this.rt = rt;
	}

	public String getRw() {
		return rw;
	}

	public void setRw(String rw) {
		this.rw = rw;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getKepemilikan() {
		return kepemilikan;
	}

	public void setKepemilikan(String kepemilikan) {
		this.kepemilikan = kepemilikan;
	}

}
